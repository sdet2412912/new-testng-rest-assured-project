package POJO1;

import java.util.List;

public class POJO_GetApi_Parent_Array {
	
	
	public int getPage() {
		return page;
	}
	public int getPer_page() {
		return per_page;
	}
	public int getTotal() {
		return total;
	}
	public int getTotal_pages() {
		return total_pages;
	}
	public List<POJO_Get_DataArray> getData() {
		return data;
	}
	public POJO_get__supports getSupport() {
		return support;
	}
	private int page;
	private int per_page;
	private int total;
	private int total_pages;
	private List<POJO_Get_DataArray> data;
	private POJO_get__supports support;
	

}
