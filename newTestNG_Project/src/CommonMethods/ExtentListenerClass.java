package CommonMethods;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentListenerClass implements ITestListener {

	ExtentSparkReporter sparkReporter;
	ExtentReports extentReports;
	ExtentTest test;

	public void reportConfigurations() {
		sparkReporter = new ExtentSparkReporter(".\\extent-report\\report.html\\");
		extentReports = new ExtentReports();

		extentReports.attachReporter(sparkReporter);
		
		// adding env information to report
		extentReports.setSystemInfo("OS", "Windows 10");
		extentReports.setSystemInfo("user", "Nikita");

		// configure to changing the look and feel the report
		sparkReporter.config().setDocumentTitle("NewTestNG_Project Extent Listner Reports");
		sparkReporter.config().setReportName("my reports");
		sparkReporter.config().setTheme(Theme.STANDARD);
	}
                                                                                                                                    
	public void onStart(ITestContext result) {
		System.out.println("on test start method");
		reportConfigurations();
	}

	public void onFinish(ITestContext result) {
		System.out.println("on test Finish method");
		extentReports.flush();

	}

	public void onTestFailure(ITestResult result) {
		System.out.println("on test failure method :" + result.getName());
		test=extentReports.createTest(result.getName());
		test.log(Status.FAIL, MarkupHelper.createLabel("on test failure method :" + result.getName(), ExtentColor.RED));
	}

	public void onTestSkipped(ITestResult result) {
		System.out.println("on test Skipped method :" + result.getName());
		test=extentReports.createTest(result.getName());
		test.log(Status.SKIP,
				MarkupHelper.createLabel("on test Skipped method :" + result.getName(), ExtentColor.YELLOW));

	}

	public void onTestStart(ITestResult result) {
		System.out.println("on test Start method :" + result.getName());
	}

	public void onTestSuccess(ITestResult result) {
		System.out.println("on test Success method :" + result.getName());
		test=extentReports.createTest(result.getName());
		test.log(Status.PASS,
				MarkupHelper.createLabel("on test failure method :" + result.getName(), ExtentColor.GREEN));

	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		System.out.println("on test Failed But Within Success Percentage method :" + result.getName());
	}

}
