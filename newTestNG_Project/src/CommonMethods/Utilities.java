package CommonMethods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Utilities {
	public static File createFolder(String foldername) {

		String project_Folder = System.getProperty("user.dir");
		System.out.println(project_Folder);

		File folder = new File(project_Folder + "\\API_Logs\\" + foldername);

		if (folder.exists()) {
			System.out.println(folder + " , is already exists in RestAssuredProject path :" + project_Folder);
			System.out.println("====================================================");

		} else {
			System.out.println(
					folder + " , Doesn't exists in RestAssuredProject :" + project_Folder + ", " + "creating the new");
			System.out.println("====================================================");

			folder.mkdir();
			System.out.println(folder + " , Created in RestAssuredProject :" + project_Folder);
		}
		return folder;

	}

	public static void createLogFile(File Filelocation, String Filename, String endpoint, String requestBody,
			String responseHeader, String responseBody) throws IOException {
		System.out.println("====================================================");

		File newTextFile = new File(Filelocation + "\\" + Filename + ".txt");
		System.out.println("File create with name :" + newTextFile.getName());

		FileWriter writedata = new FileWriter(newTextFile);
		writedata.write("Endpoint is : " + endpoint + "\n\n");
		writedata.write("Request_Body is : \n" + requestBody + "\n\n");
		writedata.write("Response header is : " + responseHeader + "\n\n");
		writedata.write("Response_Body is : " + responseBody);

		writedata.close();

	}

	public static ArrayList<String> readExcelData(String sheetname, String TestCase) throws IOException {
		ArrayList<String> arrayData = new ArrayList<String>();

		String projectFolder = System.getProperty("user.dir");
		
		//FileInputStream fis = new FileInputStream(projectFolder + "\\DataFiles\\" + "\\ExcelData.xlsx");
		
		XSSFWorkbook wb = new XSSFWorkbook(projectFolder + "\\DataFiles\\" + "\\ExcelData.xlsx");
		
		int countOfSheet = wb.getNumberOfSheets();
		 System.out.println(countOfSheet);
		
		 for (int i = 0; i < countOfSheet; i++) {

			if (wb.getSheetName(i).equals(sheetname)) {
				// System.out.println(wb.getSheetName(i));
				XSSFSheet sheet = wb.getSheetAt(i);
				
				Iterator<Row> rows = sheet.iterator();
				
				while (rows.hasNext()) {
					Row dataRows = rows.next();
					String testCaseName = dataRows.getCell(0).getStringCellValue();
					
					if (testCaseName.equals(TestCase)) {
						Iterator<Cell> cellValues = dataRows.iterator();
						while (cellValues.hasNext()) {
							String testData = "";
							Cell cell = cellValues.next();
							CellType dataType = cell.getCellType();
							if (dataType.toString().equals("STRING")) {
								testData = cell.getStringCellValue();
							} else if (dataType.toString().equals("NUMERIC")) {
								double num_testdata = cell.getNumericCellValue();
								testData = String.valueOf(num_testdata);
							}
							System.out.println(testData);
							arrayData.add(testData);

						}
						break;
					}
				}
				break;
			}

			else {
				System.out.println("no sheet found of name :" + sheetname + ", In this iteration :" + i);
			}
		}
		wb.close();
		return arrayData;

	}

}
