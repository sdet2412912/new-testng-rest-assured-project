package practice_mock;

import org.testng.annotations.Test;
import org.testng.annotations.Parameters;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

import static io.restassured.RestAssured.given;

public class Post_Result {

	String hostname = "https://reqres.in";
	String resource = "/api/users/2";
	String headername = "Content-Type";
	String headervalue = "application/json";

	@Parameters({ "reqName", "reqJob" })
	@Test
	public void test(String reqName, String reqJob) {
		Response res = RestAssured.given().header(headername, headervalue)
				.body("{\r\n" + "\"name\": \"" + reqName + "\",\r\n" + "\"job\": \"" + reqJob + "\"\r\n" + "}")
				.post(hostname + resource);

		System.out.println(res.statusCode());
		
        System.out.println(res.getBody().asString());

		/*
		 * RestAssured.baseURI = hostname; String resBody = given().header(headervalue,
		 * headername).body("{\r\n" + "    \"name\": \""+reqName+"\",\r\n" +
		 * "    \"job\": \""+reqJob+"\"\r\n" +
		 * "}").when().patch(resource).then().extract().response().asString();
		 * 
		 * 
		 * System.out.println(resBody);
		 */

	}
}