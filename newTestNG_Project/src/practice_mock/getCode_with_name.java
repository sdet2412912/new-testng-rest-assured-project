package practice_mock;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class getCode_with_name {
	@Test
	public void test() {

		String hostname = "https://reqres.in";
		String resource = "/api/users?page=2";

		RestAssured.baseURI = hostname;
		String responseBody = given().when().get(resource).then().extract().response().asString();
		// System.out.println(responseBody);

		JsonPath jp = new JsonPath(responseBody);

	String	body=jp.getString("data["+0+"]");
	System.out.println(body);
	
	int act_id=jp.getInt("data["+2+"].id");
	System.out.println(act_id);
	
	int id []= {7,8,9,10,11,12};
	int exp_id=id[2];
	Assert.assertEquals(act_id, exp_id);
	 
	String f_name[]={"Michael","Lindsay","Tobias"};
	String exp_fname= f_name[2];
	String actNam=jp.getString("data["+2+"].first_name");
	Assert.assertEquals(actNam, exp_fname);
	System.out.println("TC is passed actName and Lastname is matched");
	
	
			}
}
