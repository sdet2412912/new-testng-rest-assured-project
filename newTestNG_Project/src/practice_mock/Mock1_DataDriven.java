package practice_mock;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class Mock1_DataDriven {
	
	String hostname = "https://reqres.in";
	String resource = "/api/users/2";
	String headername = "Content-Type";
	String headervalue= "application/json";
	
	@Test(dataProvider="req")
	public void test(String reqName, String reqJob) {
	Response res = RestAssured.given().header(headername, headervalue).body("{\r\n" + "\"name\": \""+reqName+"\",\r\n" + "\"job\": \""+reqJob+"\"\r\n" + "}").post(hostname+resource);

}
	@DataProvider
    public Object[][] req() {
    	return new Object[][] {{"nik","qa"},{ "morphous", "leader" }};
    }
	
	
}