package practice_mock;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

import static io.restassured.RestAssured.given;

public class zPractice_Get_API {

	@Test
	public void getApi() {

		String hostname = "https://reqres.in";
		String resource = "/api/users?page=2";
		Response res = RestAssured.given().get(hostname + resource);
		ResponseBody resbody = res.getBody();
		
		

		int act_id = resbody.jsonPath().getInt("data[" + 0 + "].id");
		System.out.println(act_id);
		String act_name = resbody.jsonPath().getString("data[" + 0 + "].first_name");
		System.out.println(act_name);
		String act_lname = resbody.jsonPath().getString("data[" + 0 + "].last_name");
		System.out.println(act_lname);
	/*	int id[] = { 7 };
		int exp_id = id[0];
		System.out.println(exp_id);

		String exp_name = "Michael";
		System.out.println(exp_name);
		String exp_lname = "Lawson";
		System.out.println(exp_lname);
		

		Assert.assertEquals(act_id, exp_id, "print msg");
		Assert.assertEquals(act_name, exp_name, "pass");
		Assert.assertEquals(act_lname, exp_lname);
		System.out.println("assertion is pass.");*/

	}

}
