package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.TestNG_Retry_Analyzer;
import CommonMethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Patch_Test_Script extends API_Trigger {

	File logfolder;
	ResponseBody responseBody;
	Response res;

	@BeforeTest
	public void setUp() {
		logfolder = Utilities.createFolder("Patch_API");
	}

	@Test (retryAnalyzer= TestNG_Retry_Analyzer.class , description = "Validate the responseBody parameters of Patch_TC_1")


	public void validate_Patch_Method() {
		 res = API_Trigger.patch_API_Trigger(patch_requestBody(), patch_endpoint());

		 int statuscode = res.statusCode();
		 System.out.println(statuscode);
		 responseBody = res.getBody();
		 System.out.println(responseBody.asString());

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(patch_requestBody());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");

		Assert.assertEquals(res_name, req_name, "Name in Res_Body is not equal to Name sent in Res_Body");
		Assert.assertEquals(res_job, req_job, "Job in Res_Body is not equal to Job sent in Res_Body ");
		Assert.assertEquals(res_updatedAt, expecteddate, "updatedAt in Res_Body is not equal to Date Generated");

	}
	@AfterTest
	  public void tearDown() throws IOException {
		  Utilities.createLogFile(logfolder, "Patch_API_TC1", patch_endpoint(), patch_requestBody(),
					res.getHeaders().toString(), responseBody.asString());
	  }

}
