package TestScripts;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.TestNG_Retry_Analyzer;
import CommonMethods.Utilities;
import Pojo.POJO_GetApi_Parent_Array;
import Pojo.POJO_Get_DataArray;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Get_Nested_JSON extends API_Trigger {

	File logfolder;
	POJO_GetApi_Parent_Array responseBody;
	Response res;

	@BeforeTest
	public void setUp() {
		logfolder = Utilities.createFolder("Get_API");
	}

	@Test(retryAnalyzer = TestNG_Retry_Analyzer.class, description = "Validate the responseBody parameters of Get_TC_1")

	public void validate_Get_Method() {

		res =get_API_Trigger(get_endpoint());
		//System.out.println(res.asString());
		int statuscode = res.statusCode();
		responseBody=res.as(POJO_GetApi_Parent_Array.class);
		//responseBody = res.as(POJO_GetApi_Parent_Array.class);
		System.out.println(responseBody);
		int exp_page = 2;
		int exp_per_page = 6;
		int exp_total = 12;
		int exp_total_pages = 2;

		int id[] = { 7, 8, 9, 10, 11, 12 };
		String email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String first_name[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String last_name[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String avatar[] = { "https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg",
				"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg" };

		int res_page = responseBody.getPage();
		int res_per_page = responseBody.getPer_page();
		int res_total = responseBody.getTotal();
		int res_total_pages = responseBody.getTotal_pages();

		List<POJO_Get_DataArray> dataArray = responseBody.getData();
		System.out.println(dataArray);
		int sizeofarray = dataArray.size();

		Assert.assertEquals(statuscode, 200, "Correct statuscode is not found even after retrying for 5 times");

		Assert.assertEquals(res_page, exp_page);
		Assert.assertEquals(res_per_page, exp_per_page);
		Assert.assertEquals(res_total, exp_total);
		Assert.assertEquals(res_total_pages, exp_total_pages);

		for (int i = 0; i < sizeofarray; i++) {
			Assert.assertEquals(dataArray.get(i).getId(), id[i], "Validation of id failed " + i);
			Assert.assertEquals(dataArray.get(i).getEmail(), email[i], "Validation of email failed" + i);
			Assert.assertEquals(dataArray.get(i).getFirst_name(), first_name[i], "Validation of email failed" + i);
			Assert.assertEquals(dataArray.get(i).getLast_name(), last_name[i], "Validation of email failed " + i);
			Assert.assertEquals(dataArray.get(i).getAvatar(), avatar[i], "Validation of email failed" + i);

		}
		String exp_url = "https://reqres.in/#support-heading";
		String exp_text = "To keep ReqRes free, contributions towards server costs are appreciated!";

		Assert.assertEquals(responseBody.getSupport().getUrl(), exp_url, "Validation of URL failed");
		Assert.assertEquals(responseBody.getSupport().getText(), exp_text, "Validation of text failed");

	}

	@AfterTest
	public void tearDown() throws IOException {
		Utilities.createLogFile(logfolder, "Get_API_TC1", get_endpoint(), get_requestBody(),
				res.getHeaders().toString(), res.getBody().asString());

	}
}
