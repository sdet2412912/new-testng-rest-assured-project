package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import CommonMethods.API_Trigger;
import CommonMethods.TestNG_Retry_Analyzer;
import CommonMethods.Utilities;
import EnvoirnmentAndRepository.Req_Repo_POJO;
import Pojo.POJO_PostAPI;
import Pojo.POJO_PutAPI;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class POJO_Put_Test_Script extends API_Trigger {

	File logfolder;
	Response res;
	POJO_PutAPI res_Body;
	Req_Repo_POJO pj = new Req_Repo_POJO();
	@BeforeTest
	public void setUp() {
		logfolder = Utilities.createFolder("Put_API");
	}

	@Test (retryAnalyzer= TestNG_Retry_Analyzer.class , description = "Validate the responseBody parameters of Put_TC_1")

	public void validate_Put_Method() throws JsonProcessingException {
		res = API_Trigger.put_API_Trigger( pj.put_TC1(),put_endpoint());

		int statuscode = res.statusCode();
		 res_Body = res.as(POJO_PutAPI.class);
		String res_name = res_Body.getName();
		String res_job = res_Body.getJob();
		String res_updatedAt = res_Body.getUpdatedAt();
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(pj.put_TC1());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expected_date = currentdate.toString().substring(0, 11);
		Assert.assertEquals(statuscode, 200, "Correct statuscode not found even after retrying for 5 times");

		Assert.assertEquals(res_name, req_name, "Name in Res_Body is not equal to Name sent in Res_Body");
		Assert.assertEquals(res_job, req_job, "Job in Res_Body is not equal to Job sent in Res_Body ");
		Assert.assertEquals(res_updatedAt, expected_date, "updatedAt in Res_Body is not equal to Date Generated");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createLogFile( logfolder,"Put_API_TC1",put_endpoint(),pj.put_TC1(),
				res.getHeaders().toString(),res.getBody().asString());

	}

}
