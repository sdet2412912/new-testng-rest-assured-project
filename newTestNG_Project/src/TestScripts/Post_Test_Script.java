package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.TestNG_Retry_Analyzer;
import CommonMethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Post_Test_Script extends API_Trigger {

	File logfolder;
	ResponseBody responseBody;
	Response res;

@BeforeMethod
	public void setUp() {
		res = API_Trigger.post_API_Trigger(post_requestBody(), post_endpoint());

		int statuscode = res.statusCode();
		Assert.assertEquals(statuscode, 201, "Correct statuscode not found even after retrying for 5 times");


	}

	@Test (retryAnalyzer= TestNG_Retry_Analyzer.class , description = "Validate the responseBody parameters of Post_TC_1")

	public void validate_Post_Method() {
				responseBody = res.getBody();

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(post_requestBody());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		
		Assert.assertEquals(res_name, req_name, "Name in Res_Body is not equal to Name sent in Res_Body");
		Assert.assertEquals(res_job, req_job, "Job in Res_Body is not equal to Job sent in Res_Body ");
		Assert.assertNotNull(res_id, "Id in Res_Body is found to be null");
		//Assert.assertEquals(res_createdAt, expecteddate, "createdAt in Res_Body is not equal to Date Generated");

	}

	@AfterTest
	public void teardown() throws IOException {
		logfolder = Utilities.createFolder("Post_API");
		
		Utilities.createLogFile(logfolder,"Post_API_TC1", post_endpoint(), post_requestBody(),
				res.getHeaders().toString(), responseBody.asString());

	}

}
