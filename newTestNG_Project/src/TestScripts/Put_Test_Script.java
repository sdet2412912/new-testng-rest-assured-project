package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.TestNG_Retry_Analyzer;
import CommonMethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Put_Test_Script extends API_Trigger {

	File logfolder;
	ResponseBody responseBody;
	Response res;

	@BeforeTest
	public void setUp() {
		logfolder = Utilities.createFolder("Put_API");
	}

	@Test (retryAnalyzer= TestNG_Retry_Analyzer.class , description = "Validate the responseBody parameters of Put_TC_1")

	public void validate_Put_Method() {
		res = API_Trigger.put_API_Trigger(put_requestBody(), put_endpoint());

		int statuscode = res.statusCode();
		responseBody = res.getBody();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(put_requestBody());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expected_date = currentdate.toString().substring(0, 11);
		Assert.assertEquals(statuscode, 200, "Correct statuscode not found even after retrying for 5 times");

		Assert.assertEquals(res_name, req_name, "Name in Res_Body is not equal to Name sent in Res_Body");
		Assert.assertEquals(res_job, req_job, "Job in Res_Body is not equal to Job sent in Res_Body ");
		Assert.assertEquals(res_updatedAt, expected_date, "updatedAt in Res_Body is not equal to Date Generated");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createLogFile( logfolder,"Put_API_TC1",put_endpoint(), put_requestBody(),
				res.getHeaders().toString(), responseBody.asString());

	}

}
