package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.TestNG_Retry_Analyzer;
import CommonMethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Post_DataProvider extends API_Trigger {

	File logfolder;
	ResponseBody responseBody;
	Response res;

	@DataProvider
	public Object [][] req_BodyData() {
		return new Object[][] { { "morphous", "leader" }, { "nikita", "QA" } };
		
		//Object [][] obj1 =new Object [2][2];
		
		//Object [][] obj ={ { "morphous", "leader" }, { "nikita", "QA" }};
		//return obj;
	}

	@BeforeTest
	public void setUp() {
		logfolder = Utilities.createFolder("Post_API");
	}

	@Test(retryAnalyzer = TestNG_Retry_Analyzer.class, dataProvider = "req_BodyData", description = "Validate the responseBody parameters of Post_TC_1")

	public void validate_Post_Method(String req_Name, String req_Job) {
		String req_body = "{\r\n" + "\"name\": \"" + req_Name + "\",\r\n" + "\"job\": \"" + req_Job + "\"\r\n" + "}";
		res = API_Trigger.post_API_Trigger(req_body, post_endpoint());

		int statuscode = res.statusCode();
		responseBody = res.getBody();
		System.out.println("============" + responseBody.asString());
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);
		JsonPath jsp_req = new JsonPath(post_requestBody());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(statuscode, 201, "Correct statuscode not found even after retrying for 5 times");

		Assert.assertEquals(res_name, req_Name, "Name in Res_Body is not equal to Name sent in Res_Body");
		Assert.assertEquals(res_job, req_Job, "Job in Res_Body is not equal to Job sent in Res_Body ");
		Assert.assertNotNull(res_id, "Id in Res_Body is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in Res_Body is not equal to Date Generated");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createLogFile(logfolder, "Post_API_TC1", post_endpoint(), post_requestBody(),
				res.getHeaders().toString(), responseBody.asString());

	}

}
