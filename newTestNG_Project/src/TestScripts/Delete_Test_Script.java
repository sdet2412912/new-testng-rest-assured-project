package TestScripts;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.TestNG_Retry_Analyzer;
import CommonMethods.Utilities;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Delete_Test_Script extends API_Trigger {

	File logfolder;
	Response res;

	@BeforeTest
	public void setUp() {
		logfolder = Utilities.createFolder("Delete_API");
	}

	@Test (retryAnalyzer= TestNG_Retry_Analyzer.class , description = "Validate the responseBody parameters of Delete_TC_1")

	public void validate_Delete_Method() {
		res = API_Trigger.delete_API_Trigger(delete_requestBody(), delete_endpoint());

		int statuscode = res.statusCode();

		Assert.assertEquals(statuscode, 204, "Correct status code not found even after retrying for 5 times");

	}

	@AfterTest
	public void tearDown() throws IOException {

		Utilities.createLogFile(logfolder, "Delete_API_TC1", delete_endpoint(), delete_requestBody(),
				res.getHeaders().toString(), res.asString());

	}

}
