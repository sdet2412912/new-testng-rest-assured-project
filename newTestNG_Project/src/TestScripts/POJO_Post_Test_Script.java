package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import CommonMethods.API_Trigger;
import CommonMethods.TestNG_Retry_Analyzer;
import CommonMethods.Utilities;
import EnvoirnmentAndRepository.Req_Repo_POJO;
import Pojo.POJO_PostAPI;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class POJO_Post_Test_Script extends API_Trigger {

	File logfolder;
	Response res;
	POJO_PostAPI res_Body;

	Req_Repo_POJO pj = new Req_Repo_POJO();

	@BeforeMethod
	public void setUp() throws JsonProcessingException {

		res = API_Trigger.post_API_Trigger(pj.post_TC1(), post_endpoint());

		int statuscode = res.statusCode();

		res_Body = res.as(POJO_PostAPI.class);
		Assert.assertEquals(statuscode, 201, "Correct statuscode not found even after retrying for 5 times");

	}

	@Test(retryAnalyzer = TestNG_Retry_Analyzer.class, description = "Validate the responseBody parameters of Post_TC_1")

	public void validate_Post_Method() throws JsonProcessingException {
		String res_name = res_Body.getName();
		String res_job = res_Body.getJob();
		String res_id = res_Body.getId();
		String res_createdAt = res_Body.getCreatedAt();
		res_createdAt = res_createdAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(pj.post_TC1());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name, "Name in Res_Body is not equal to Name sent in Res_Body");
		Assert.assertEquals(res_job, req_job, "Job in Res_Body is not equal to Job sent in Res_Body ");
		Assert.assertNotNull(res_id, "Id in Res_Body is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in Res_Body is not equal to Date Generated");

	}

	@AfterTest
	public void teardown() throws IOException {
		logfolder = Utilities.createFolder("Post_API");

		Utilities.createLogFile(logfolder, "Post_API_TC1", post_endpoint(), pj.post_TC1(), res.getHeaders().toString(),
				res.getBody().asString());

	}

}
