package EnvoirnmentAndRepository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import Pojo.POJO_PatchAPI;
import Pojo.POJO_PostAPI;
import Pojo.POJO_PutAPI;

public class Req_Repo_POJO {

	ObjectMapper OM = new ObjectMapper();
	POJO_PostAPI post_api = new POJO_PostAPI();
	POJO_PutAPI put_api = new POJO_PutAPI();
	POJO_PatchAPI patch_api = new POJO_PatchAPI();

	public String post_TC1() throws JsonProcessingException {

		post_api.setName("Nikita");
		post_api.setJob("SR.QA");

		String reuestBody = OM.writeValueAsString(post_api);
		return reuestBody;

	}
    public String put_TC1() throws JsonProcessingException {

		put_api.setName("Nilesh");
		put_api.setJob("Lead");

		String reuestBody = OM.writeValueAsString(put_api);
		return reuestBody;
	}
    public String patch_TC1() throws JsonProcessingException {

		patch_api.setName("Nidhish");
		patch_api.setJob("PO");

		String reuestBody = OM.writeValueAsString(patch_api);
		return reuestBody;
	}
}
