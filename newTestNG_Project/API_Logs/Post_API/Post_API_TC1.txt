Endpoint is : https://reqres.in/api/users

Request_Body is : 
{"name":"Nikita","job":"SR.QA","id":null,"createdAt":null}

Response header is : Date=Fri, 24 May 2024 07:47:54 GMT
Content-Type=application/json; charset=utf-8
Content-Length=80
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1716536874&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=EN3kGF6ZH%2F5TqUe3nPWbvF3%2ByVk%2F23fe7sqokb72Yhs%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1716536874&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=EN3kGF6ZH%2F5TqUe3nPWbvF3%2ByVk%2F23fe7sqokb72Yhs%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"50-MM89X8Dxbiv2gr3Kwk6ccYiYtgE"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Server=cloudflare
CF-RAY=888bae2abc2c4475-BOM

Response_Body is : {"name":"Nikita","job":"SR.QA","id":"72","createdAt":"2024-05-24T07:47:54.907Z"}